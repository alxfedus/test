import request from '@/helpers/request';
import addQueryToUrl from '@/helpers/addQueryToUrl';

const api = {
  getFilmsQuery: '/movies',
  getFilmQuery: id => `/movies?movie_id=${id}`,
  getFilmsShowsQuery: '/movieShows',
  getFilmShowsQuery: id => `/movieShows?movie_id=${id}`,
  getSearchQuery: query => addQueryToUrl(query, '/movies/find'),
};

export default {
  getFilms: () => request.get(api.getFilmsQuery),
  getFilm: ({ id }) => request.get(api.getFilmQuery(id)),
  getFilmsShows: () => request.get(api.getFilmsShowsQuery),
  getFilmShows: ({ id }) => request.get(api.getFilmShowsQuery(id)),
  getSearchFilms: query => request.get(api.getSearchQuery(query)),
};
