import Vue from 'vue';
import Router from 'vue-router';
import common from '@/router/common';

Vue.use(Router);

const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    ...common,
  ],
});

export default router;
