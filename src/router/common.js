import Home from '@/components/views/Home.vue';

export default [
  {
    path: '/',
    name: 'home',
    component: Home,
  },
  {
    path: '/:id',
    name: 'film',
    props: true,
    component: () => import(
      /* webpackChunkName: "FilmView" */
      '@/components/views/FilmView.vue'
    ),
  },
];
