import Vue from 'vue';
import moment from 'moment';

// eslint-disable-next-line
Vue.filter('date', function (value, format = 'MM/DD/YYYY') {
  if (value) return moment(value).format(format);
});
