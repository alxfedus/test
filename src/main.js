import Vue from 'vue';
import vuetify from '@/plugins/vuetify';
import App from '@/components/views/App.vue';
import router from '@/router';

import '@/filters';

Vue.config.productionTip = false;
Vue.prototype.$filters = Vue.options.filters;

new Vue({
  vuetify,
  router,
  render: h => h(App),
}).$mount('#app');
