export default (str) => {
  const obj = {};
  str.toLowerCase().split('').forEach((char) => {
    obj[char] = obj[char] + 1 || 1;
  });
  const maxValue = Math.max(...Object.values(obj));
  // eslint-disable-next-line
  return Object.entries(obj).find(([key, val]) => val === maxValue)[0];
};
