const addQueryToUrl = (query, url) => {
  if (!query) return url;

  let queryString = '?';

  Object.entries(query).forEach(
    ([key, value]) => {
      queryString += `${key}=${value}&`;
    },
  );
  queryString = queryString.slice(0, -1);

  return url + queryString;
};

export default addQueryToUrl;
