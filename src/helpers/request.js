import axios from 'axios';
import settings from '@/settings';

const request = axios.create({
  baseURL: settings.baseUrl,
  headers: {
    'Content-Type': 'application/json',
  },
});

export default request;
