export default (url) => {
  const obj = {};
  const pairs = url.split('?')[1].split('&');
  pairs.forEach((pair) => {
    const [key, value] = pair.split('=');
    obj[key] = value;
  });
  return obj;
};
