async function fetchApiData(
  state = {},
  method,
  params = {},
  transformData = data => data,
  showPending = true,
) {
  const result = new Promise(async (resolve, reject) => {
    try {
      state.isPending = showPending;
      const { data } = await method(params);
      state.data = transformData(data);
      resolve({ data });
    } catch (error) {
      state.error = error;
      reject(error);
    } finally {
      state.isPending = false;
    }
  });

  return result;
}

export default fetchApiData;
